"""The core of Emily."""

import asyncio
from collections import defaultdict
import logging
from taillight.signal import Signal


logging.basicConfig(level=logging.DEBUG)


in_plugins = []
in_proto_plugins = defaultdict(list)
in_protocols = set()
in_proto_insts = []
out_plugins = []

event_loop = asyncio.get_event_loop()


push_event = Signal(('source', 'push'))
"""A push occurred.

Caller is project name (group if available, otherwise 'default').

Push details are:

* User {name, email}
* Commits [{id,msg,url,author:{name,email}}]
* Branch
"""
merge_event = Signal(('source', 'mr'))
"""A merge request has been created.

Caller is project name (group if available, otherwise 'default').

Details are:

* User {name}
* Source branch
* URL
"""
issue_open_event = Signal(('issue', 'open'))
issue_comment_event = Signal(('issue', 'comment'))
issue_close_event = Signal(('issue', 'close'))
pipe_event = Signal(('source', 'pipeline'))
"""A pipeline has changed status.

Caller is project name (group if available, otherwise 'default').

Details are:

* Pipeline ID #
* Source branch
* Status
* URL
"""



def register_in(plugin):
    inst = plugin()

    for protocol in plugin.PROTOCOLS:
        in_protocols.add(protocol)
        in_proto_insts.append(protocol(event_loop))

    in_plugins.append(inst)


def register_out(plugin):
    out_plugins.append(plugin(event_loop))


def run():
    coros = []
    for proto in in_proto_insts:
        coros += proto.coros()
    for out in out_plugins:
        coros += out.coros()

    asyncio.gather(*coros, loop=event_loop)
    event_loop.run_forever()
